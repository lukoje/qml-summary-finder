from PySide2 import QtCore, QtGui, QtWidgets, QtQml

from src.config import *
from src.finder import Finder
from src.utils import SummaryField

import os
import sys
import subprocess


class FileListModel(QtCore.QAbstractListModel):

    FileNameRole = QtCore.Qt.UserRole + 1
    FilePathRole = QtCore.Qt.UserRole + 2

    def __init__(self, summaries, parent=None):
        super().__init__(parent)
        self.summaries = summaries

    def data(self, index, role=QtCore.Qt.DisplayRole):
        row = index.row()
        if role == FileListModel.FileNameRole:
            return self.summaries[row].file_name
        if role == FileListModel.FilePathRole:
            return self.summaries[row].file_path

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.summaries)

    def resetItems(self, summaries):
        self.beginResetModel()
        self.summaries = summaries
        self.endResetModel()

    def roleNames(self):
        return {
            FileListModel.FileNameRole: b'file_name',
            FileListModel.FilePathRole: b'file_path'
        }


class UIApp(QtCore.QObject):
    def __init__(self):
        super(UIApp, self).__init__()

        self._engine = QtQml.QQmlApplicationEngine()
        self._engine.rootContext().setContextProperty(
            'APPLICATION_VERSION', APPLICATION_VERSION)

        self._engine.load(os.path.join(BASE_DIR, 'ui', 'App.qml'))
        self._window = self._engine.rootObjects()[0]

        self._tf_search = self._window.findChild(
            QtCore.QObject, 'tf_search')

        self._lv_files = self._window.findChild(
            QtCore.QObject, 'lv_files')

        self._pb_status = self._window.findChild(
            QtCore.QObject, 'pb_status')

        self._tf_position_filter = self._window.findChild(
            QtCore.QObject, 'tf_position_filter')

        self._tf_education_filter = self._window.findChild(
            QtCore.QObject, 'tf_education_filter')

        self._tf_academic_degree_filter = self._window.findChild(
            QtCore.QObject, 'tf_academic_degree_filter')

        self._tf_academic_title_filter = self._window.findChild(
            QtCore.QObject, 'tf_academic_title_filter')

        self._tf_experience_filter = self._window.findChild(
            QtCore.QObject, 'tf_experience_filter')

        # Personal Information

        self._tf_first_name_filter = self._window.findChild(
            QtCore.QObject, 'tf_first_name_filter')

        self._tf_last_name_filter = self._window.findChild(
            QtCore.QObject, 'tf_last_name_filter')

        self._tf_middle_name_filter = self._window.findChild(
            QtCore.QObject, 'tf_middle_name_filter')

        self._tf_birthday_filter = self._window.findChild(
            QtCore.QObject, 'tf_birthday_filter')

        self._tf_sex_filter = self._window.findChild(
            QtCore.QObject, 'tf_sex_filter')

        # Finder
        self.finder = Finder()
        self.finder.load_model()

        self._lv_files_model = FileListModel(self.finder.model.summaries)
        self._lv_files.setProperty('model', self._lv_files_model)

        self._connect()

    def _connect(self):
        self.window.filters_update.connect(self.filters_update)
        self.window.search_update.connect(self.update_search)
        self.window.open_file.connect(self.open_file)

    @property
    def window(self):
        return self._window

    @property
    def search_field(self):
        return self._tf_search

    @property
    def files_list(self):
        return self._lv_files

    @property
    def progress_status(self):
        return self._pb_status

    @property
    def tf_position_filter(self):
        return self._tf_position_filter

    @property
    def tf_education_filter(self):
        return self._tf_education_filter

    @property
    def tf_academic_degree_filter(self):
        return self._tf_academic_degree_filter

    @property
    def tf_academic_title_filter(self):
        return self._tf_academic_title_filter

    @property
    def tf_experience_filter(self):
        return self._tf_experience_filter

    # Personal Information

    @property
    def tf_first_name_filter(self):
        return self._tf_first_name_filter

    @property
    def tf_last_name_filter(self):
        return self._tf_last_name_filter

    @property
    def tf_middle_name_filter(self):
        return self._tf_middle_name_filter

    @property
    def tf_birthday_filter(self):
        return self._tf_birthday_filter

    @property
    def tf_sex_filter(self):
        return self._tf_sex_filter

    @QtCore.Slot()
    def filters_update(self):

        position_field = SummaryField('position', str(
            self.tf_position_filter.property('text')).lower())

        education_field = SummaryField('education', str(
            self.tf_education_filter.property('text')).lower()
        )

        academec_degree_field = SummaryField('academic_degree', str(
            self.tf_academic_degree_filter.property('text')).lower()
        )

        academic_title_field = SummaryField('academic_title', str(
            self.tf_academic_title_filter.property('text')).lower()
        )

        experience_field = SummaryField('experience', str(
            self.tf_experience_filter.property('text')).lower()
        )

        # Personal Information
        first_name_field = SummaryField('first_name', str(
            self.tf_first_name_filter.property('text')).lower()
        )

        last_name_field = SummaryField('last_name', str(
            self.tf_last_name_filter.property('text')).lower()
        )

        middle_name_field = SummaryField('middle_name', str(
            self.tf_middle_name_filter.property('text')).lower()
        )

        birthday_field = SummaryField('birthday', str(
            self.tf_birthday_filter.property('text')).lower()
        )

        sex_field = SummaryField('sex',
                                 str(self.tf_sex_filter.property('text')).lower()
                                 )

        fields = [
            position_field,
            education_field,
            academec_degree_field,
            academic_title_field,
            experience_field,

            first_name_field,
            last_name_field,
            middle_name_field,
            birthday_field,
            sex_field
        ]

        fields = list(
            filter(lambda field: field.value not in ['', ' ', 'нет'], fields))
        summaries = self.finder.search_by_fields(fields)
        self._lv_files_model.resetItems(summaries)

    @QtCore.Slot(str)
    def open_file(self, file_path):
        if sys.platform == 'Darwin':
            subprocess.call(('open', file_path))
        elif sys.platform == 'Windows':
            os.startfile(file_path)
        else:
            subprocess.call(('xdg-open', file_path))

    @QtCore.Slot(str)
    def update_search(self, search_string):
        tags = list(filter(lambda s: bool(s.isalnum()),
                    search_string.split(',')))
        tags = [i.lower() for i in tags]
        summaries = self.finder.search_by_tags(tags)
        self._lv_files_model.resetItems(summaries)
