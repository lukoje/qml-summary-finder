import os
import re
import pickle
import hashlib

from typing import NamedTuple, Dict, List, Tuple
from docx import Document


class SummaryField(NamedTuple):
    field_name: str
    value: str


class Summary(NamedTuple):
    file_path: str
    file_name: str
    created_date: str
    summary_fields: List[SummaryField]
    fields_tags: List[str]


class SummaryModel(object):
    def __init__(self):
        self._summaries: Summary = []
        self._model_hash: str = ''

    @property
    def summaries(self):
        return self._summaries

    @property
    def summaries_count(self):
        return len(self.summaries)

    @property
    def model_hash(self):
        return self._model_hash

    @model_hash.setter
    def model_hash(self, val):
        self._model_hash = val

    def add_summary(self, summary: Summary):
        self.summaries.append(summary)

    def del_summary(self, summary: Summary):
        self.summaries.remove(summary)

    def get_summaries_by_field(self, field: SummaryField) -> List[Summary]:
        return list(filter(lambda summary: field in summary.summary_fields, self.summaries))

    def get_summaries_by_fields(self, fields: SummaryField) -> List[Summary]:
        return list(filter(lambda summary: set(fields) < set(summary.summary_fields), self.summaries))

    def get_summaries_by_tags(self, tags: List[str]) -> List[Summary]:
        return list(filter(
            lambda summary: bool(set(tags) < set(summary.fields_tags)), self.summaries))


def get_dir_size(directory: str, suffix: str = 'docx') -> int:
    """
    Return the total size (in bytes) of all files in the input directory.
    """
    size = 0
    for dirpath, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith(suffix):
                size += os.path.getsize(os.path.join(dirpath, filename))
    return size


def get_dir_files(directory: str, suffix: str = 'docx') -> List:
    """
        Return the list of files inside in the directory
    """
    files = []
    for dirpath, dirnames, filenames in os.walk(directory):
        for filename in filenames:
            if filename.endswith(suffix):
                files.append(os.path.join(dirpath, filename))
    return files


def calculate_hash(directory: str, suffix: str = 'docx') -> str:
    """
        Calculates the total hash of files in a directory
    """
    files = get_dir_files(directory, suffix='docx')
    md5 = hashlib.md5()
    for _file in files:
        with open(_file, 'rb') as f:
            md5.update(f.read())
    return md5.hexdigest()


def is_cache_actual(directory: str, cache_hash: SummaryModel, suffix: str = 'docx') -> bool:
    """
        Checks file hash and model hash 
    """
    dir_hash = calculate_hash(directory, suffix)
    return dir_hash == cache_hash


def parse_docx_summary(file_path: str, fields: List = None, sep: str = ':') -> Summary:
    """
        Parse docx summary file and return Summary object
    """
    doc = Document(file_path)

    path = file_path
    file_name = os.path.basename(path)
    created_date = os.path.getmtime(path)
    summary_fields = []
    fields_tags = []

    for p in doc.paragraphs:
        text = p.text
        if text and text not in ['', ' ', '\n']:
            text_split = re.split(r'\s*:\s*', text)
            if len(text_split) > 1:
                f, v = text_split

                if v.lower() not in ['', ' ', 'нет']:
                    fields_tags.append(v.lower())

                for sum_field in SUMMARY_FIELD_SLUGS:
                    if f.lower() in SUMMARY_FIELD_SLUGS[sum_field]:
                        summary_fields.append(
                            SummaryField(sum_field, v.lower()))

    return Summary(
        file_path=path,
        file_name=file_name,
        created_date=created_date,
        summary_fields=summary_fields,
        fields_tags=fields_tags
    )


def create_cache(cache_path: str, obj=object) -> int:
    """
        Creates a cache file with a model object 
    """
    with open(cache_path, 'wb') as cache:
        pickle.dump(obj, cache)
    return 0


def read_cache(cache_path: str) -> object:
    """
        Read a cache file with a model object 
    """
    with open(cache_path, 'rb') as cache:
        obj = pickle.load(cache)
        return obj


def cache_exists(cache_path: str) -> bool:
    """
        Checks if a file exists 
    """
    return True if os.path.exists(cache_path) else False


SUMMARY_FIELD_SLUGS = {
    'position': ('на должность', 'должность'),
    'first_name': ('имя'),
    'last_name': ('фамилия'),
    'middle_name': ('отчество'),
    'birthday': ('дата рождения'),
    'sex': ('пол (мужской, женский)'),
    'education': ('образование (высшее, среднее, начальное)'),
    'academic_degree': ('ученая степень (нет, кандидат наук, доктор наук)'),
    'academic_title': ('ученое звание (нет, доцент, профессор)'),
    'experience': ('стаж работы по специальности (если нет стажа по специальности, поставьте – 0)'),
}
