import os
import sys
import logging

from src.config import DOCX_DIR, CACHE_FILE_PATH
from src.utils import *


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('Finder')


class Finder(object):
    """
    Finder
    """

    def __init__(self, model=None):
        self.model = model or SummaryModel()

    def load_model(self, directory=DOCX_DIR):
        """
            Try to load summaries from cache file. 
        """
        if cache_exists(CACHE_FILE_PATH):
            logger.debug(f'Нашел кеш файл {CACHE_FILE_PATH}')
            cached_model = read_cache(CACHE_FILE_PATH)
            logger.debug('Проверяю его актуальность...')
            if is_cache_actual(directory, cached_model.model_hash):
                logger.debug('Кеш актуален, использую его')
                self.model = cached_model
                logger.debug(
                    f'Прочитал из кеша {self.model.summaries_count} записей')
                return
            else:
                logger.debug('Кеш файл не актуален')
                logger.debug('Создаю модель...')
                self.create_model()
                logger.debug(f'Создал: {self.model.summaries_count} записей')
        else:
            logger.debug('Не могу найти кеш файл')
            logger.debug('Создаю модель...')
            self.create_model()
            logger.debug(
                f'Прочитал из кеша {self.model.summaries_count} записей')

    def create_model(self, directory=DOCX_DIR) -> int:
        """
            Load all docx files and save them to the model. 
        """

        dir_docx_size = get_dir_size(directory, suffix='docx')
        dir_docx_files = get_dir_files(directory, suffix='docx')
        dir_docx_hash = calculate_hash(directory, suffix='docx')

        for _file in dir_docx_files:
            summary = parse_docx_summary(_file)
            if summary:
                self.model.add_summary(summary)

        # Create cache
        self.model.model_hash = dir_docx_hash
        create_cache(CACHE_FILE_PATH, self.model)

        return 0

    def search_by_fields(self, fields: List[SummaryField], *args, **kwargs) -> List[Summary]:
        return self.model.get_summaries_by_fields(fields)

    def search_by_tags(self, tags: List[str], *args, **kwargs) -> List[Summary]:
        return self.model.get_summaries_by_tags(tags)
