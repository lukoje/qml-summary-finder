import QtQuick 2.5
import QtQuick.Layouts 1.11
import QtQuick.Controls 2.15


ApplicationWindow {

    id: appWindow
    visible: true
    color: "#ffffff"
    width: 800
    height: 500

    title: "Summary Finder"

    signal search_update(string search_string)
    signal filters_update()
    signal open_file(string file_path)


    readonly property bool inPortrait: appWindow.width - 100 < appWindow.height

    property int found_files_count: 0

    ProgressBar {
        id: pb_status
        visible: false
        objectName: "pb_status"
        width: appWindow.width
        y: tf_search.height
        z: 1
        value: 0.5

    }

    TextField {
        id: tf_search
        objectName: "tf_search"
        horizontalAlignment: Text.AlignLeft
        renderType: Text.NativeRendering
        hoverEnabled: true
        placeholderText: "Найти"
        selectByMouse: true
        focus: true
        z: 1
        width: parent.width
        parent: appWindow.overlay

        onTextChanged: search_update(tf_search.text)
    }

    Drawer {
        id:drawer

        y: tf_search.height
        width: inPortrait ? 0.70 * appWindow.width : 0.30 * appWindow.width
        height: appWindow.height - tf_search.height

        modal: inPortrait
        interactive: inPortrait
        position: inPortrait ? 0 : 1
        visible: !inPortrait

        Flickable {
            id: flickable

            anchors.fill: parent

            topMargin: 20
            bottomMargin: 20
            contentHeight: column.height


            Column {

                id: column
                spacing: 20

                anchors.margins: 20
                anchors.left: parent.left
                anchors.right: parent.right

                width: parent.width


                Label {
                    width: parent.width
                    font.pixelSize: 24
                    elide: Label.ElideRight
                    horizontalAlignment: Qt.AlignHCenter
                    text: qsTr("Фильтры")
                }



                TextField {
                    id: tf_position_filter
                    objectName: "tf_position_filter"
                    width: parent.width
                    placeholderText: "На должность"
                    selectByMouse: true

                    onTextChanged: filters_update()
                }
                TextField {
                    id: tf_education_filter
                    objectName: "tf_education_filter"
                    width: parent.width
                    placeholderText: "Образование (высшее, среднее, начальное)"
                    selectByMouse: true

                    onTextChanged: filters_update()
                }
                TextField {
                    id: tf_academic_degree_filter
                    objectName: "tf_academic_degree_filter"
                    width: parent.width
                    placeholderText: "Ученая степень (нет, кандидат наук, доктор наук)"
                    selectByMouse: true

                    onTextChanged: filters_update()
                }
                TextField {
                    id: tf_academic_title_filter
                    objectName: "tf_academic_title_filter"
                    width: parent.width
                    placeholderText: "Ученое звание (нет, доцент, профессор)"
                    selectByMouse: true

                    onTextChanged: filters_update()
                }
                TextField {
                    id: tf_experience_filter
                    objectName: "tf_experience_filter"
                    width: parent.width
                    placeholderText: "Стаж работы по специальности"
                    selectByMouse: true

                    onTextChanged: filters_update()
                }

                ToolSeparator {
                    width: parent.width
                    orientation: Qt.Horizontal
                }

                TextField {
                    id: tf_first_name_filter
                    objectName: "tf_first_name_filter"
                    width: parent.width
                    placeholderText: "Имя"
                    selectByMouse: true

                    onTextChanged: filters_update()
                }
                TextField {
                    id: tf_last_name_filter
                    objectName: "tf_last_name_filter"
                    width: parent.width
                    placeholderText: "Фамилия"
                    selectByMouse: true

                    onTextChanged: filters_update()
                }
                TextField {
                    id: tf_middle_name_filter
                    objectName: "tf_middle_name_filter"
                    width: parent.width
                    placeholderText: "Отчество"
                    selectByMouse: true

                    onTextChanged: filters_update()
                }

                TextField {
                    id: tf_birthday_filter
                    objectName: "tf_birthday_filter"
                    width: parent.width
                    placeholderText: "Дата рождения: дд.мм.гг"
                    selectByMouse: true
                    validator: RegExpValidator {
                        regExp: /^\d{2}([./-])\d{2}\1\d{4}$/
                    }

                    onTextChanged: filters_update()
                }

                TextField {
                    id: tf_sex_filter
                    objectName: "tf_sex_filter"
                    width: parent.width
                    placeholderText: "Пол (мужской, женский)"
                    selectByMouse: true
                    validator: RegExpValidator {
                        regExp: /^мужской|женский$/
                    }

                    onTextChanged: filters_update()
                }

            }

            ScrollIndicator.vertical: ScrollIndicator { }
        }

    }


    ListView {
        id: lv_files
        objectName: "lv_files"
        visible: true

        anchors.fill: parent
        clip: true
        anchors.topMargin: tf_search.height
        anchors.leftMargin: !inPortrait ? drawer.width : undefined

        topMargin: 20
        bottomMargin: 20

        headerPositioning: ListView.OverlayHeader

        transform: Translate {
            x:inPortrait ? drawer.position * flick_welcome_column.width * 0.66 : 0
        }

        header: Pane {
            id: lv_files_header
            z: 2
            width: parent.width


            Label {
                width: parent.width
                horizontalAlignment: Text.AlignHCenter
                text: "..."

            }

            MenuSeparator {
                parent: lv_files_header
                width: parent.width
                anchors.verticalCenter: parent.bottom
                visible: !lv_files.atYBeginning
            }
        }

        delegate: ItemDelegate {

            width: appWindow.width

            Row {
                spacing: 20
                width: parent.width

                Image {
                    fillMode: Image.Stretch
                    width: 32
                    height: 32
                    source: "../resources/doc.svg"
                }

                Label {
                    text: file_name
                    font.pixelSize: 16
                }

            }

            onDoubleClicked: function() {
                console.log("Open File:", file_path)
                open_file(file_path)
            }
        }
    }

    Flickable {
        id: flick_welcome
        visible: false

        anchors.fill: parent
        anchors.topMargin: tf_search.height
        anchors.leftMargin: !inPortrait ? drawer.width : undefined

        topMargin: 20
        bottomMargin: 20
        contentHeight: column.height



        Column {
            id: flick_welcome_column

            spacing: 20
            anchors.margins: 20
            anchors.left: parent.left
            anchors.right: parent.right

            Label {
                font.pixelSize: 22
                width: parent.width
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                text: qsTr("Добро пожаловать в Summary Finder")
            }

            Label {
                width: parent.width
                wrapMode: Label.WordWrap
                text: qsTr("Чтобы найти нужное резюме достаточно перечислить нужные вам критерии "+
                           "или воспользоваться фильтрам в боковом меню\n\n")
            }
        }
    }
}



/*##^##
Designer {
    D{i:0;formeditorZoom:0.75}
}
##^##*/
